# Mitarbeit 

## Fehler melden

Wenn Sie Fehler in den Regeln finden, dann können Sie unter [Issues > New Issue](https://gitlab.opencode.de/xleitstelle/xplanung/validierungsregeln/standard/-/issues/new) einen Fehlerreport erstellen. Eine Übersicht aller bereits erstellten Fehlerberichte finden Sie ebenfalls unter [Issues](https://gitlab.opencode.de/xleitstelle/xplanung/validierungsregeln/standard/-/issues).

## Änderungen 

Wenn Sie Fehler in den Regeln entdecken, dann stellen Sie bitte Ihre Änderungen als [Merge Request](https://gitlab.opencode.de/xleitstelle/xplanung/validierungsregeln/standard/-/merge_requests) zur Verfügung. 
