(:
  #%L
  XPlanung-Validierungsregeln - Implementierung der Konformitätsregeln des XPlanung-Standards als XQuery-Anweisungen
  %%
  Copyright (C) 2019 lat/lon GmbH, info@lat-lon.de, www.lat-lon.de
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-2.1.html>.
  #L%
  :)
declare default element namespace 'http://www.xplanung.de/xplangml/5/4';
declare namespace gml='http://www.opengis.net/gml/3.2';

for $h in //FP_BebauungsFlaeche/sonderNutzung
where (
	(
		(
			$h/text() = '1000' or
			$h/text() = '1100' or 
			$h/text() = '1200' or 
			$h/text() = '1400'
		)
		and (
			$h/../besondereArtDerBaulNutzung and
			$h/../besondereArtDerBaulNutzung/text() != '2000'
		)
	)
	or
	(
		(
			$h/text() = '1500' or 
			$h/text() = '1600' or 
			$h/text() = '16000' or 
			$h/text() = '16001' or 
			$h/text() = '16002' or 
			$h/text() = '1700' or 
			$h/text() = '1800' or 
			$h/text() = '1900' or 
			$h/text() = '2000' or 
			$h/text() = '2100' or 
			$h/text() = '2200' or 
			$h/text() = '2300' or
			$h/text() = '23000' or
			$h/text() = '2400' or 
			$h/text() = '2500' or 
			$h/text() = '2600' or 
			$h/text() = '2700' or
			$h/text() = '2720' or
			$h/text() = '2800' or 
			$h/text() = '2900' or 
			$h/text() = '9999'
		)
		and (
			$h/../besondereArtDerBaulNutzung and
			$h/../besondereArtDerBaulNutzung/text() != '2100'
		)
	)
)
return $h/../@gml:id/string()
