(:
  #%L
  XPlanung-Validierungsregeln - Implementierung der Konformitätsregeln des XPlanung-Standards als XQuery-Anweisungen
  %%
  Copyright (C) 2019 lat/lon GmbH, info@lat-lon.de, www.lat-lon.de
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-2.1.html>.
  #L%
  :)
declare default element namespace 'http://www.xplanung.de/xplangml/4/1';
declare namespace gml = 'http://www.opengis.net/gml/3.2';


for $h in //*[matches(name(), 'BP_Flaechenschlussobjekt|BP_Ueberlagerungsobjekt|BP_AbgrabungsFlaeche|BP_AufschuettungsFlaeche|BP_BodenschaetzeFlaeche|BP_RekultivierungsFlaeche|BP_AbstandsFlaeche|BP_BaugebietsTeilFlaeche|BP_BesondererNutzungszweckFlaeche|BP_FoerderungsFlaeche|BP_GebaeudeFlaeche|BP_GemeinschaftsanlagenFlaeche|BP_GemeinschaftsanlagenZuordnung|BP_NebenanlagenAusschlussFlaeche|BP_NebenanlagenFlaeche|BP_PersGruppenBestimmteFlaeche|BP_RegelungVergnuegungsstaetten|BP_SpezielleBauweise|BP_UeberbaubareGrundstuecksFlaeche|BP_DenkmalschutzEinzelanlage|BP_DenkmalschutzEnsembleFlaeche|BP_ErhaltungsBereichFlaeche|BP_GemeinbedarfsFlaeche|BP_SpielSportanlagenFlaeche|BP_GruenFlaeche|BP_KleintierhaltungFlaeche|BP_Landwirtschaft|BP_WaldFlaeche|BP_AnpflanzungBindungErhaltung|BP_AusgleichsFlaeche|BP_AusgleichsMassnahme|BP_EingriffsBereich|BP_Schutzgebiet|BP_SchutzPflegeEntwicklungsFlaeche|BP_SchutzPflegeEntwicklungsMassnahme|BP_AbstandsMass|BP_FestsetzungNachLandesrecht|BP_FreiFlaeche|BP_GenerischesObjekt|BP_HoehenMass|BP_KennzeichnungsFlaeche|BP_TextlicheFestsetzungsFlaeche|BP_UnverbindlicheVormerkung|BP_Veraenderungssperre|BP_Wegerecht|BP_ErneuerbareEnergieFlaeche|BP_Immissionsschutz|BP_LuftreinhalteFlaeche|BP_VerEntsorgung|BP_Strassenkoerper|BP_StrassenVerkehrsFlaeche|BP_VerkehrsflaecheBesondererZweckbestimmung|BP_GewaesserFlaeche|BP_WasserwirtschaftsFlaeche|FP_Flaechenschlussobjekt|FP_Ueberlagerungsobjekt|FP_Abgrabung|FP_AbgrabungsFlaeche|FP_Aufschuettung|FP_AufschuettungsFlaeche|FP_Bodenschaetze|FP_BodenschaetzeFlaeche|FP_BebauungsFlaeche|FP_KeineZentrAbwasserBeseitigungFlaeche|FP_AnpassungKlimawandel|FP_Gemeinbedarf|FP_SpielSportanlage|FP_Gruen|FP_LandwirtschaftsFlaeche|FP_WaldFlaeche|FP_AusgleichsFlaeche|FP_SchutzPflegeEntwicklung|FP_GenerischesObjekt|FP_Kennzeichnung|FP_NutzungsbeschraenkungsFlaeche|FP_PrivilegiertesVorhaben|FP_TextlicheDarstellungsFlaeche|FP_UnverbindlicheVormerkung|FP_VorbehalteFlaeche|FP_VerEntsorgung|FP_ZentralerVersorgungsbereich|FP_Strassenverkehr|FP_Gewaesser|FP_Wasserwirtschaft|SO_Bodenschutzrecht|SO_Denkmalschutzrecht|SO_Forstrecht|SO_Luftverkehrsrecht|SO_Schienenverkehrsrecht|SO_SonstigesRecht|SO_Strassenverkehrsrecht|SO_Wasserrecht|SO_SchutzgebietNaturschutzrecht|SO_SchutzgebietSonstigesRecht|SO_SchutzgebietWasserrecht|SO_Gebiet') and position]
where (
    $h/position/gml:Polygon or
    $h/position/gml:Surface or
    $h/position/gml:MultiSurface or
    $h/position/gml:LinearRing or
    $h/position/gml:PolygonPatch or
    $h/position/gml:Ring
)
and not (
    $h/flaechenschluss
)
return $h/@gml:id/string()