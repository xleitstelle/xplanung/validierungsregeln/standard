(:
  #%L
  XPlanung-Validierungsregeln - Implementierung der Konformitätsregeln des XPlanung-Standards als XQuery-Anweisungen
  %%
  Copyright (C) 2019 lat/lon GmbH, info@lat-lon.de, www.lat-lon.de
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-2.1.html>.
  #L%
  :)
declare default element namespace 'http://www.xplanung.de/xplangml/4/1';
declare namespace xplan='http://www.xplanung.de/xplangml/4/1';
declare namespace gml='http://www.opengis.net/gml/3.2';
declare namespace wfs='http://www.opengis.net/wfs';
declare namespace xlink='http://www.w3.org/1999/xlink';
declare namespace xsi='http://www.w3.org/2001/XMLSchema-instance';

(
  every $h in //BP_Baugebiet[besondereArtDerBaulNutzung/text() = '1000'] satisfies
  not(exists($h/allgArtDerBaulNutzung)) or $h/allgArtDerBaulNutzung/text() = '1000'
)
and
(
  every $h in //BP_Baugebiet[besondereArtDerBaulNutzung/text() = '1100'] satisfies
  not(exists($h/allgArtDerBaulNutzung)) or $h/allgArtDerBaulNutzung/text() = '1000'
)
and
(
  every $h in //BP_Baugebiet[besondereArtDerBaulNutzung/text() = '1200'] satisfies
  not(exists($h/allgArtDerBaulNutzung)) or $h/allgArtDerBaulNutzung/text() = '1000'
)
and
(
  every $h in //BP_Baugebiet[besondereArtDerBaulNutzung/text() = '1300'] satisfies
  not(exists($h/allgArtDerBaulNutzung)) or $h/allgArtDerBaulNutzung/text() = '1000'
)
and
(
  every $h in //BP_BaugebietsTeilFlaeche[besondereArtDerBaulNutzung/text() = '1000'] satisfies
  not(exists($h/allgArtDerBaulNutzung)) or $h/allgArtDerBaulNutzung/text() = '1000'
)
and
(
  every $h in //BP_BaugebietsTeilFlaeche[besondereArtDerBaulNutzung/text() = '1100'] satisfies
  not(exists($h/allgArtDerBaulNutzung)) or $h/allgArtDerBaulNutzung/text() = '1000'
)
and
(
  every $h in //BP_BaugebietsTeilFlaeche[besondereArtDerBaulNutzung/text() = '1200'] satisfies
  not(exists($h/allgArtDerBaulNutzung)) or $h/allgArtDerBaulNutzung/text() = '1000'
)
and
(
  every $h in //BP_BaugebietsTeilFlaeche[besondereArtDerBaulNutzung/text() = '1300'] satisfies
  not(exists($h/allgArtDerBaulNutzung)) or $h/allgArtDerBaulNutzung/text() = '1000'
)
and
(
  every $h in //BP_Baugebiet[besondereArtDerBaulNutzung/text() = '1400'] satisfies
  not(exists($h/allgArtDerBaulNutzung)) or $h/allgArtDerBaulNutzung/text() = '2000'
)
and
(
  every $h in //BP_Baugebiet[besondereArtDerBaulNutzung/text() = '1500'] satisfies
  not(exists($h/allgArtDerBaulNutzung)) or $h/allgArtDerBaulNutzung/text() = '2000'
)
and
(
  every $h in //BP_Baugebiet[besondereArtDerBaulNutzung/text() = '1600'] satisfies
  not(exists($h/allgArtDerBaulNutzung)) or $h/allgArtDerBaulNutzung/text() = '2000'
)
and
(
  every $h in //BP_BaugebietsTeilFlaeche[besondereArtDerBaulNutzung/text() = '1400'] satisfies
  not(exists($h/allgArtDerBaulNutzung)) or $h/allgArtDerBaulNutzung/text() = '2000'
)
and
(
  every $h in //BP_BaugebietsTeilFlaeche[besondereArtDerBaulNutzung/text() = '1500'] satisfies
  not(exists($h/allgArtDerBaulNutzung)) or $h/allgArtDerBaulNutzung/text() = '2000'
)
and
(
  every $h in //BP_BaugebietsTeilFlaeche[besondereArtDerBaulNutzung/text() = '1600'] satisfies
  not(exists($h/allgArtDerBaulNutzung)) or $h/allgArtDerBaulNutzung/text() = '2000'
)
and
(
  every $h in //BP_Baugebiet[besondereArtDerBaulNutzung/text() = '1700'] satisfies
  not(exists($h/allgArtDerBaulNutzung)) or $h/allgArtDerBaulNutzung/text() = '3000'
)
and
(
  every $h in //BP_Baugebiet[besondereArtDerBaulNutzung/text() = '1800'] satisfies
  not(exists($h/allgArtDerBaulNutzung)) or $h/allgArtDerBaulNutzung/text() = '3000'
)
and
(
  every $h in //BP_BaugebietsTeilFlaeche[besondereArtDerBaulNutzung/text() = '1700'] satisfies
  not(exists($h/allgArtDerBaulNutzung)) or $h/allgArtDerBaulNutzung/text() = '3000'
)
and
(
  every $h in //BP_BaugebietsTeilFlaeche[besondereArtDerBaulNutzung/text() = '1800'] satisfies
  not(exists($h/allgArtDerBaulNutzung)) or $h/allgArtDerBaulNutzung/text() = '3000'
)
and
(
  every $h in //BP_Baugebiet[besondereArtDerBaulNutzung/text() = '2000'] satisfies
  not(exists($h/allgArtDerBaulNutzung)) or $h/allgArtDerBaulNutzung/text() = '4000'
)
and
(
  every $h in //BP_Baugebiet[besondereArtDerBaulNutzung/text() = '2100'] satisfies
  not(exists($h/allgArtDerBaulNutzung)) or $h/allgArtDerBaulNutzung/text() = '4000'
)
and
(
  every $h in //BP_Baugebiet[besondereArtDerBaulNutzung/text() = '3000'] satisfies
  not(exists($h/allgArtDerBaulNutzung)) or $h/allgArtDerBaulNutzung/text() = '4000'
)
and
(
  every $h in //BP_Baugebiet[besondereArtDerBaulNutzung/text() = '4000'] satisfies
  not(exists($h/allgArtDerBaulNutzung)) or $h/allgArtDerBaulNutzung/text() = '4000'
)
and
(
  every $h in //BP_BaugebietsTeilFlaeche[besondereArtDerBaulNutzung/text() = '2000'] satisfies
  not(exists($h/allgArtDerBaulNutzung)) or $h/allgArtDerBaulNutzung/text() = '4000'
)
and
(
  every $h in //BP_BaugebietsTeilFlaeche[besondereArtDerBaulNutzung/text() = '2100'] satisfies
  not(exists($h/allgArtDerBaulNutzung)) or $h/allgArtDerBaulNutzung/text() = '4000'
)
and
(
  every $h in //BP_BaugebietsTeilFlaeche[besondereArtDerBaulNutzung/text() = '3000'] satisfies
  not(exists($h/allgArtDerBaulNutzung)) or $h/allgArtDerBaulNutzung/text() = '4000'
)
and
(
  every $h in //BP_BaugebietsTeilFlaeche[besondereArtDerBaulNutzung/text() = '4000'] satisfies
  not(exists($h/allgArtDerBaulNutzung)) or $h/allgArtDerBaulNutzung/text() = '4000'
)
