(:
  #%L
  XPlanung-Validierungsregeln - Implementierung der Konformitätsregeln des XPlanung-Standards als XQuery-Anweisungen
  %%
  Copyright (C) 2019 lat/lon GmbH, info@lat-lon.de, www.lat-lon.de
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-2.1.html>.
  #L%
  :)
declare default element namespace 'http://www.xplanung.de/xplangml/4/1';
declare namespace xplan='http://www.xplanung.de/xplangml/4/1';
declare namespace gml='http://www.opengis.net/gml/3.2';
declare namespace wfs='http://www.opengis.net/wfs';
declare namespace xlink='http://www.w3.org/1999/xlink';
declare namespace xsi='http://www.w3.org/2001/XMLSchema-instance';

(
  every $h in //FP_Strassenverkehr[besondereZweckbestimmung/text() = '14000'] satisfies
  not(exists($h/zweckbestimmung)) or $h/zweckbestimmung/text() = '1400'
)
and
(
  every $h in //FP_Strassenverkehr[besondereZweckbestimmung/text() = '14001'] satisfies
  not(exists($h/zweckbestimmung)) or $h/zweckbestimmung/text() = '1400'
)
and
(
  every $h in //FP_Strassenverkehr[besondereZweckbestimmung/text() = '14002'] satisfies
  not(exists($h/zweckbestimmung)) or $h/zweckbestimmung/text() = '1400'
)
and
(
  every $h in //FP_Strassenverkehr[besondereZweckbestimmung/text() = '14003'] satisfies
  not(exists($h/zweckbestimmung)) or $h/zweckbestimmung/text() = '1400'
)
and
(
  every $h in //FP_Strassenverkehr[besondereZweckbestimmung/text() = '14004'] satisfies
  not(exists($h/zweckbestimmung)) or $h/zweckbestimmung/text() = '1400'
)
and
(
  every $h in //FP_Strassenverkehr[besondereZweckbestimmung/text() = '14005'] satisfies
  not(exists($h/zweckbestimmung)) or $h/zweckbestimmung/text() = '1400'
)
and
(
  every $h in //FP_Strassenverkehr[besondereZweckbestimmung/text() = '14006'] satisfies
  not(exists($h/zweckbestimmung)) or $h/zweckbestimmung/text() = '1400'
)
and
(
  every $h in //FP_Strassenverkehr[besondereZweckbestimmung/text() = '14007'] satisfies
  not(exists($h/zweckbestimmung)) or $h/zweckbestimmung/text() = '1400'
)
and
(
  every $h in //FP_Strassenverkehr[besondereZweckbestimmung/text() = '14008'] satisfies
  not(exists($h/zweckbestimmung)) or $h/zweckbestimmung/text() = '1400'
)
and
(
  every $h in //FP_Strassenverkehr[besondereZweckbestimmung/text() = '14009'] satisfies
  not(exists($h/zweckbestimmung)) or $h/zweckbestimmung/text() = '1400'
)
and
(
  every $h in //FP_Strassenverkehr[besondereZweckbestimmung/text() = '14010'] satisfies
  not(exists($h/zweckbestimmung)) or $h/zweckbestimmung/text() = '1400'
)
and
(
  every $h in //FP_Strassenverkehr[besondereZweckbestimmung/text() = '1401'] satisfies
  not(exists($h/zweckbestimmung)) or $h/zweckbestimmung/text() = '1400'
)
