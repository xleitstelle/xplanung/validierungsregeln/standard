(:
  #%L
  XPlanung-Validierungsregeln - Implementierung der Konformitätsregeln des XPlanung-Standards als XQuery-Anweisungen
  %%
  Copyright (C) 2019 lat/lon GmbH, info@lat-lon.de, www.lat-lon.de
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-2.1.html>.
  #L%
  :)
declare default element namespace 'http://www.xplanung.de/xplangml/4/1';
declare namespace xplan='http://www.xplanung.de/xplangml/4/1';
declare namespace gml='http://www.opengis.net/gml/3.2';
declare namespace wfs='http://www.opengis.net/wfs';
declare namespace xlink='http://www.w3.org/1999/xlink';
declare namespace xsi='http://www.w3.org/2001/XMLSchema-instance';

declare variable $weitereBesondweitereZweckbestimmung as xs:string := '0';
declare variable $weitereZweckbestimmung as xs:string := '0';

(
  every $h in //FP_PrivilegiertesVorhaben[weitereBesondweitereZweckbestimmung1] satisfies
  
  (: Wenn weitereBesondweitereZweckbestimmung aus Ziffern besteht, :)
  if (
    every $weitereBesondweitereZweckbestimmung in $h/weitereBesondweitereZweckbestimmung1/text() satisfies
    matches($weitereBesondweitereZweckbestimmung,"[0-9]")
  )
  (: dann überprüfe, ob weitereZweckbestimmung ebenfalls aus Ziffern besteht. :)
  then
  
    (: Wenn weitereZweckbestimmung aus Ziffern besteht, :)
    if (
      every $weitereZweckbestimmung in $h/weitereZweckbestimmung1/text() satisfies
      matches($weitereZweckbestimmung,"[0-9]")
    )
    (: dann überprüfe, ob weitereZweckbestimmung in weitereBesondweitereZweckbestimmung enthalten ist. :)
    then
      contains($weitereBesondweitereZweckbestimmung, $weitereZweckbestimmung) or not(exists($h/weitereZweckbestimmung1))
    else boolean('false')
    
  else boolean('false')
)
and
(
  every $h in //FP_PrivilegiertesVorhaben[weitereBesondweitereZweckbestimmung2] satisfies
  
  (: Wenn weitereBesondweitereZweckbestimmung aus Ziffern besteht, :)
  if (
    every $weitereBesondweitereZweckbestimmung in $h/weitereBesondweitereZweckbestimmung2/text() satisfies
    matches($weitereBesondweitereZweckbestimmung,"[0-9]")
  )
  (: dann überprüfe, ob weitereZweckbestimmung ebenfalls aus Ziffern besteht. :)
  then
  
    (: Wenn weitereZweckbestimmung aus Ziffern besteht, :)
    if (
      every $weitereZweckbestimmung in $h/weitereZweckbestimmung2/text() satisfies
      matches($weitereZweckbestimmung,"[0-9]")
    )
    (: dann überprüfe, ob weitereZweckbestimmung in weitereBesondweitereZweckbestimmung enthalten ist. :)
    then
      contains($weitereBesondweitereZweckbestimmung, $weitereZweckbestimmung) or not(exists($h/weitereZweckbestimmung2))
    else boolean('false')
    
  else boolean('false')
)
