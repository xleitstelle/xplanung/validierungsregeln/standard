(:
  #%L
  XPlanung-Validierungsregeln - Implementierung der Konformitätsregeln des XPlanung-Standards als XQuery-Anweisungen
  %%
  Copyright (C) 2019 lat/lon GmbH, info@lat-lon.de, www.lat-lon.de
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-2.1.html>.
  #L%
  :)
declare default element namespace 'http://www.xplanung.de/xplangml/5/2';
declare namespace gml='http://www.opengis.net/gml/3.2';

for $h in //*[matches(local-name(), 'BP_AbstandsFlaeche|BP_AbweichungVonUeberbaubererGrundstuecksFlaeche|BP_EingriffsBereich|BP_ErhaltungsBereichFlaeche|BP_FoerderungsFlaeche|BP_FreiFlaeche|BP_GebaeudeFlaeche|BP_GemeinschaftsanlagenFlaeche|BP_NebenanlagenAusschlussFlaeche|BP_NebenanlagenFlaeche|BP_NichtUeberbaubareGrundstuecksflaeche|BP_PersGruppenBestimmteFlaeche|BP_RegelungVergnuegungsstaetten|BP_Sichtflaeche|BP_SpezielleBauweise|BP_TechnischeMassnahmenFlaeche|BP_TextlicheFestsetzungsFlaeche|BP_UeberbaubareGrundstuecksFlaeche|BP_Veraenderungssperre|BP_ZusatzkontingentLaermFlaeche')]
where not ($h/flaechenschluss/text() = 'false')
return $h/@gml:id/string()
