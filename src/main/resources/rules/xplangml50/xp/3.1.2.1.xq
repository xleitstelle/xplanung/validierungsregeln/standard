(:
  #%L
  XPlanung-Validierungsregeln - Implementierung der Konformitätsregeln des XPlanung-Standards als XQuery-Anweisungen
  %%
  Copyright (C) 2019 lat/lon GmbH, info@lat-lon.de, www.lat-lon.de
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-2.1.html>.
  #L%
  :)
declare default element namespace 'http://www.xplanung.de/xplangml/5/0';
declare namespace gml='http://www.opengis.net/gml/3.2';
declare namespace xlink='http://www.w3.org/1999/xlink';

for $h in //praesentationsobjekt
let $id := substring($h/@xlink:href/string(),2)
where not(
	(//XP_FPO[@gml:id eq $id]) or
	(//XP_LPO[@gml:id eq $id]) or
	(//XP_PPO[@gml:id eq $id]) or
	(//XP_Praesentationsobjekt[@gml:id eq $id]) or
	(//XP_LTO[@gml:id eq $id]) or
	(//XP_PTO[@gml:id eq $id]) or
	(//XP_Nutzungsschablone[@gml:id eq $id])
)
return $id
