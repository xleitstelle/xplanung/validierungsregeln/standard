(:
  #%L
  XPlanung-Validierungsregeln - Implementierung der Konformitätsregeln des XPlanung-Standards als XQuery-Anweisungen
  %%
  Copyright (C) 2019 lat/lon GmbH, info@lat-lon.de, www.lat-lon.de
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-2.1.html>.
  #L%
  :)
declare default element namespace 'http://www.xplanung.de/xplangml/4/0';
declare namespace xplan='http://www.xplanung.de/xplangml/4/0';
declare namespace gml='http://www.opengis.net/gml/3.2';
declare namespace wfs='http://www.opengis.net/wfs';
declare namespace xlink='http://www.w3.org/1999/xlink';
declare namespace xsi='http://www.w3.org/2001/XMLSchema-instance';

declare variable $besondereZweckbestimmung as xs:string := '0';
declare variable $zweckbestimmung as xs:string := '0';

(
  every $h in //BP_GemeinbedarfsFlaeche[besondereZweckbestimmung] satisfies
  
  (: Wenn besondereZweckbestimmung aus Ziffern besteht, :)
  if (
    every $besondereZweckbestimmung in $h/besondereZweckbestimmung/text() satisfies
    matches($besondereZweckbestimmung,"[0-9]")
  )
  (: dann überprüfe, ob zweckbestimmung ebenfalls aus Ziffern besteht. :)
  then
  
    (: Wenn zweckbestimmung aus Ziffern besteht, :)
    if (
      every $zweckbestimmung in $h/zweckbestimmung/text() satisfies
      matches($zweckbestimmung,"[0-9]")
    )
    (: dann überprüfe, ob zweckbestimmung in besondereZweckbestimmung enthalten ist. :)
    then
      contains($besondereZweckbestimmung, $zweckbestimmung) or not(exists($h/zweckbestimmung))
    else boolean('false')
    
  else boolean('false')
)
