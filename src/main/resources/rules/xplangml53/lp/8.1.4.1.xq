(:
  #%L
  XPlanung-Validierungsregeln - Implementierung der Konformitätsregeln des XPlanung-Standards als XQuery-Anweisungen
  %%
  Copyright (C) 2019 lat/lon GmbH, info@lat-lon.de, www.lat-lon.de
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-2.1.html>.
  #L%
  :)
declare default element namespace 'http://www.xplanung.de/xplangml/5/3';
declare namespace gml='http://www.opengis.net/gml/3.2';

for $h in //*[matches(name(), 'LP_AllgGruenflaeche|LP_TextlicheFestsetzungsFlaeche|LP_ZuBegruenendeGrundstueckflaeche')]
where not (
	$h/position/gml:Polygon or
	$h/position/gml:Surface or
	$h/position/gml:MultiSurface or
	$h/position/gml:LinearRing or
	$h/position/gml:PolygonPatch or
	$h/position/gml:Ring
)
return $h/@gml:id/string()
