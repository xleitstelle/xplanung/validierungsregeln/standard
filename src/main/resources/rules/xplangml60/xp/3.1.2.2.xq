(:
  #%L
  XPlanung-Validierungsregeln - Implementierung der Konformitätsregeln des XPlanung-Standards als XQuery-Anweisungen
  %%
  Copyright (C) 2022 lat/lon GmbH, info@lat-lon.de, www.lat-lon.de
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-2.1.html>.
  #L%
  :)
declare default element namespace 'http://www.xplanung.de/xplangml/6/0';
declare namespace gml = 'http://www.opengis.net/gml/3.2';
declare namespace xlink = 'http://www.w3.org/1999/xlink';

for $h in //planinhalt
let $id := substring($h/@xlink:href/string(),2)
where not(
	//BP_AbgrabungsFlaeche[@gml:id eq $id] or
	//BP_AbstandsFlaeche[@gml:id eq $id] or
	//BP_AbstandsMass[@gml:id eq $id] or
	//BP_AbweichungVonBaugrenze[@gml:id eq $id] or
	//BP_AbweichungVonUeberbaubarerGrundstuecksFlaeche[@gml:id eq $id] or
	//BP_AnpflanzungBindungErhaltung[@gml:id eq $id] or
	//BP_AufschuettungsFlaeche[@gml:id eq $id] or
	//BP_AusgleichsFlaeche[@gml:id eq $id] or
	//BP_AusgleichsMassnahme[@gml:id eq $id] or
	//BP_BaugebietsTeilFlaeche[@gml:id eq $id] or
	//BP_BauGrenze[@gml:id eq $id] or
	//BP_BauLinie[@gml:id eq $id] or
	//BP_BereichOhneEinAusfahrtLinie[@gml:id eq $id] or
	//BP_BesondererNutzungszweckFlaeche[@gml:id eq $id] or
	//BP_EinfahrtPunkt[@gml:id eq $id] or
	//BP_EinfahrtsbereichLinie[@gml:id eq $id] or
	//BP_EingriffsBereich[@gml:id eq $id] or
	//BP_FestsetzungNachLandesrecht[@gml:id eq $id] or
	//BP_Flaechenschlussobjekt[@gml:id eq $id] or
	//BP_FlaecheOhneFestsetzung[@gml:id eq $id] or
	//BP_FoerderungsFlaeche[@gml:id eq $id] or
	//BP_FreiFlaeche[@gml:id eq $id] or
	//BP_GebaeudeFlaeche[@gml:id eq $id] or
	//BP_GebaeudeStellung[@gml:id eq $id] or
	//BP_GemeinbedarfsFlaeche[@gml:id eq $id] or
	//BP_GemeinschaftsanlagenFlaeche[@gml:id eq $id] or
	//BP_GemeinschaftsanlagenZuordnung[@gml:id eq $id] or
	//BP_GenerischesObjekt[@gml:id eq $id] or
	//BP_GruenFlaeche[@gml:id eq $id] or
	//BP_HoehenMass[@gml:id eq $id] or
	//BP_Immissionsschutz[@gml:id eq $id] or
	//BP_KennzeichnungsFlaeche[@gml:id eq $id] or
	//BP_KleintierhaltungFlaeche[@gml:id eq $id] or
	//BP_LandwirtschaftsFlaeche[@gml:id eq $id] or
	//BP_NebenanlagenAusschlussFlaeche[@gml:id eq $id] or
	//BP_NebenanlagenFlaeche[@gml:id eq $id] or
	//BP_NichtUeberbaubareGrundstuecksflaeche[@gml:id eq $id] or
	//BP_NutzungsartenGrenze[@gml:id eq $id] or
	//BP_PersGruppenBestimmteFlaeche[@gml:id eq $id] or
	//BP_RegelungVergnuegungsstaetten[@gml:id eq $id] or
	//BP_RichtungssektorGrenze[@gml:id eq $id] or
	//BP_SchutzPflegeEntwicklungsFlaeche[@gml:id eq $id] or
	//BP_SchutzPflegeEntwicklungsMassnahme[@gml:id eq $id] or
	//BP_SpezielleBauweise[@gml:id eq $id] or
	//BP_SpielSportanlagenFlaeche[@gml:id eq $id] or
	//BP_StrassenbegrenzungsLinie[@gml:id eq $id] or
	//BP_Strassenkoerper[@gml:id eq $id] or
	//BP_TechnischeMassnahmenFlaeche[@gml:id eq $id] or
	//BP_TextAbschnittFlaeche[@gml:id eq $id] or
	//BP_UeberbaubareGrundstuecksFlaeche[@gml:id eq $id] or
	//BP_UnverbindlicheVormerkung[@gml:id eq $id] or
	//BP_Veraenderungssperre[@gml:id eq $id] or
	//BP_VerEntsorgung[@gml:id eq $id] or
	//BP_WaldFlaeche[@gml:id eq $id] or
	//BP_Wegerecht[@gml:id eq $id] or
	//BP_WohngebaeudeFlaeche[@gml:id eq $id] or
	//BP_ZentralerVersorgungsbereich[@gml:id eq $id] or
	//BP_ZusatzkontingentLaerm[@gml:id eq $id] or
	//BP_ZusatzkontingentLaermFlaeche[@gml:id eq $id] or
	//FP_Abgrabung[@gml:id eq $id] or
	//FP_AnpassungKlimawandel[@gml:id eq $id] or
	//FP_Aufschuettung[@gml:id eq $id] or
	//FP_AusgleichsFlaeche[@gml:id eq $id] or
	//FP_BebauungsFlaeche[@gml:id eq $id] or
	//FP_DarstellungNachLandesrecht[@gml:id eq $id] or
	//FP_Flaechenschlussobjekt[@gml:id eq $id] or
	//FP_FlaecheOhneDarstellung[@gml:id eq $id] or
	//FP_Gemeinbedarf[@gml:id eq $id] or
	//FP_GenerischesObjekt[@gml:id eq $id] or
	//FP_Gruen[@gml:id eq $id] or
	//FP_KeineZentrAbwasserBeseitigungFlaeche[@gml:id eq $id] or
	//FP_Kennzeichnung[@gml:id eq $id] or
	//FP_Landwirtschaft[@gml:id eq $id] or
	//FP_Nutzungsbeschraenkung[@gml:id eq $id] or
	//FP_PrivilegiertesVorhaben[@gml:id eq $id] or
	//FP_SchutzPflegeEntwicklung[@gml:id eq $id] or
	//FP_SpielSportanlage[@gml:id eq $id] or
	//FP_TextAbschnittFlaeche[@gml:id eq $id] or
	//FP_UnverbindlicheVormerkung[@gml:id eq $id] or
	//FP_VerEntsorgung[@gml:id eq $id] or
	//FP_VorbehalteFlaeche[@gml:id eq $id] or
	//FP_WaldFlaeche[@gml:id eq $id] or
	//FP_ZentralerVersorgungsbereich[@gml:id eq $id] or
	//LP_BiotopverbundBiotopvernetzung[@gml:id eq $id] or
	//LP_Eingriffsregelung[@gml:id eq $id] or
	//LP_GenerischesObjekt[@gml:id eq $id] or
	//LP_SchutzBestimmterTeileVonNaturUndLandschaft[@gml:id eq $id] or
	//LP_TextAbschnittObjekt[@gml:id eq $id] or
	//LP_ZieleErfordernisseMassnahmen[@gml:id eq $id] or
	//RP_Achse[@gml:id eq $id] or
	//RP_Bodenschutz[@gml:id eq $id] or
	//RP_Einzelhandel[@gml:id eq $id] or
	//RP_Energieversorgung[@gml:id eq $id] or
	//RP_Entsorgung[@gml:id eq $id] or
	//RP_Erholung[@gml:id eq $id] or
	//RP_ErneuerbareEnergie[@gml:id eq $id] or
	//RP_Forstwirtschaft[@gml:id eq $id] or
	//RP_Freiraum[@gml:id eq $id] or
	//RP_Funktionszuweisung[@gml:id eq $id] or
	//RP_GenerischesObjekt[@gml:id eq $id] or
	//RP_Gewaesser[@gml:id eq $id] or
	//RP_Grenze[@gml:id eq $id] or
	//RP_GruenzugGruenzaesur[@gml:id eq $id] or
	//RP_Hochwasserschutz[@gml:id eq $id] or
	//RP_IndustrieGewerbe[@gml:id eq $id] or
	//RP_Klimaschutz[@gml:id eq $id] or
	//RP_Kommunikation[@gml:id eq $id] or
	//RP_Kulturlandschaft[@gml:id eq $id] or
	//RP_LaermschutzBauschutz[@gml:id eq $id] or
	//RP_Landwirtschaft[@gml:id eq $id] or
	//RP_Luftverkehr[@gml:id eq $id] or
	//RP_NaturLandschaft[@gml:id eq $id] or
	//RP_NaturschutzrechtlichesSchutzgebiet[@gml:id eq $id] or
	//RP_Planungsraum[@gml:id eq $id] or
	//RP_RadwegWanderweg[@gml:id eq $id] or
	//RP_Raumkategorie[@gml:id eq $id] or
	//RP_Rohstoff[@gml:id eq $id] or
	//RP_Schienenverkehr[@gml:id eq $id] or
	//RP_Siedlung[@gml:id eq $id] or
	//RP_SonstigeInfrastruktur[@gml:id eq $id] or
	//RP_SonstigerFreiraumschutz[@gml:id eq $id] or
	//RP_SonstigerSiedlungsbereich[@gml:id eq $id] or
	//RP_SonstVerkehr[@gml:id eq $id] or
	//RP_SozialeInfrastruktur[@gml:id eq $id] or
	//RP_Sperrgebiet[@gml:id eq $id] or
	//RP_Sportanlage[@gml:id eq $id] or
	//RP_Strassenverkehr[@gml:id eq $id] or
	//RP_Verkehr[@gml:id eq $id] or
	//RP_Wasserschutz[@gml:id eq $id] or
	//RP_Wasserverkehr[@gml:id eq $id] or
	//RP_Wasserwirtschaft[@gml:id eq $id] or
	//RP_WohnenSiedlung[@gml:id eq $id] or
	//RP_ZentralerOrt[@gml:id eq $id] or
	//SO_Baubeschraenkung[@gml:id eq $id] or
	//SO_Bodenschutzrecht[@gml:id eq $id] or
	//SO_Denkmalschutzrecht[@gml:id eq $id] or
	//SO_Forstrecht[@gml:id eq $id] or
	//SO_Gebiet[@gml:id eq $id] or
	//SO_Gelaendemorphologie[@gml:id eq $id] or
	//SO_Gewaesser[@gml:id eq $id] or
	//SO_Grenze[@gml:id eq $id] or
	//SO_Luftverkehrsrecht[@gml:id eq $id] or
	//SO_Objekt[@gml:id eq $id] or
	//SO_Schienenverkehrsrecht[@gml:id eq $id] or
	//SO_SchutzgebietWasserrecht[@gml:id eq $id] or
	//SO_Sichtflaeche[@gml:id eq $id] or
	//SO_SonstigesRecht[@gml:id eq $id] or
	//SO_Strassenverkehr[@gml:id eq $id] or
	//SO_TextAbschnittFlaeche[@gml:id eq $id] or
	//SO_Wasserrecht[@gml:id eq $id] or
	//SO_Wasserwirtschaft[@gml:id eq $id]
)
return $id
