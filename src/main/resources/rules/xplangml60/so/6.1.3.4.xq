(:
  #%L
  XPlanung-Validierungsregeln - Implementierung der Konformitätsregeln des XPlanung-Standards als XQuery-Anweisungen
  %%
  Copyright (C) 2023 lat/lon GmbH, info@lat-lon.de, www.lat-lon.de
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-2.1.html>.
  #L%
  :)
declare default element namespace 'http://www.xplanung.de/xplangml/6/0';
declare namespace gml='http://www.opengis.net/gml/3.2';
declare namespace xlink='http://www.w3.org/1999/xlink';

for $h in //*[matches(local-name(), 'SO_Objekt|SO_Flaechenobjekt|SO_Gebiet|SO_Sichtflaeche|SO_TextAbschnittFlaeche|SO_Geometrieobjekt|SO_Baubeschraenkung|SO_Bodenschutzrecht|SO_Denkmalschutzrecht|SO_Forstrecht|SO_Gelaendemorphologie|SO_Gewaesser|SO_Luftverkehrsrecht|SO_Schienenverkehrsrecht|SO_SchutzgebietWasserrecht|SO_SonstigesRecht|SO_Strassenverkehr|SO_Wasserrecht|SO_Wasserwirtschaft|SO_Linienobjekt|SO_Grenze|SO_Punktobjekt')]
where (
	$h/wirdAusgeglichenDurchBPMassnahme
	and (
	  not(//BP_Bereich[ @gml:id eq substring ( //BP_AusgleichsMassnahme[@gml:id eq substring ($h/wirdAusgeglichenDurchBPMassnahme/@xlink:href/string(),2)]/gehoertZuBereich/@xlink:href/string(),2)])
	)
)
return $h/@gml:id/string()


