(:
  #%L
  XPlanung-Validierungsregeln - Implementierung der Konformitätsregeln des XPlanung-Standards als XQuery-Anweisungen
  %%
  Copyright (C) 2022 lat/lon GmbH, info@lat-lon.de, www.lat-lon.de
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-2.1.html>.
  #L%
  :)
declare default element namespace 'http://www.xplanung.de/xplangml/6/0';
declare namespace gml='http://www.opengis.net/gml/3.2';

for $elem in //BP_BaugebietsTeilFlaeche[count(sondernutzung) > 1]
let $val := $elem/sondernutzung/BP_KomplexeSondernutzung/allgemein/string()
let $countList1 := count(
    $elem/sondernutzung/BP_KomplexeSondernutzung/allgemein/text() = '1000' or
    $elem/sondernutzung/BP_KomplexeSondernutzung/allgemein/text() = '1100' or 
    $elem/sondernutzung/BP_KomplexeSondernutzung/allgemein/text() = '1200' or 
    $elem/sondernutzung/BP_KomplexeSondernutzung/allgemein/text() = '1300' or
    $elem/sondernutzung/BP_KomplexeSondernutzung/allgemein/text() = '1400'
  )
let $countList2 := count(
    $elem/sondernutzung/BP_KomplexeSondernutzung/allgemein/text() = '1500' or 
    $elem/sondernutzung/BP_KomplexeSondernutzung/allgemein/text() = '1600' or
    $elem/sondernutzung/BP_KomplexeSondernutzung/allgemein/text() = '1650' or
    $elem/sondernutzung/BP_KomplexeSondernutzung/allgemein/text() = '16000' or 
    $elem/sondernutzung/BP_KomplexeSondernutzung/allgemein/text() = '16001' or 
    $elem/sondernutzung/BP_KomplexeSondernutzung/allgemein/text() = '16002' or 
    $elem/sondernutzung/BP_KomplexeSondernutzung/allgemein/text() = '1700' or 
    $elem/sondernutzung/BP_KomplexeSondernutzung/allgemein/text() = '1800' or 
    $elem/sondernutzung/BP_KomplexeSondernutzung/allgemein/text() = '1900' or 
    $elem/sondernutzung/BP_KomplexeSondernutzung/allgemein/text() = '2000' or 
    $elem/sondernutzung/BP_KomplexeSondernutzung/allgemein/text() = '2100' or 
    $elem/sondernutzung/BP_KomplexeSondernutzung/allgemein/text() = '2200' or 
    $elem/sondernutzung/BP_KomplexeSondernutzung/allgemein/text() = '2300' or
    $elem/sondernutzung/BP_KomplexeSondernutzung/allgemein/text() = '23000' or
    $elem/sondernutzung/BP_KomplexeSondernutzung/allgemein/text() = '2400' or 
    $elem/sondernutzung/BP_KomplexeSondernutzung/allgemein/text() = '2500' or 
    $elem/sondernutzung/BP_KomplexeSondernutzung/allgemein/text() = '2600' or 
    $elem/sondernutzung/BP_KomplexeSondernutzung/allgemein/text() = '2700' or
    $elem/sondernutzung/BP_KomplexeSondernutzung/allgemein/text() = '2720' or
    $elem/sondernutzung/BP_KomplexeSondernutzung/allgemein/text() = '2800' or 
    $elem/sondernutzung/BP_KomplexeSondernutzung/allgemein/text() = '2900' or 
    $elem/sondernutzung/BP_KomplexeSondernutzung/allgemein/text() = '9999'
  )
where ($countList1 > 0 and $countList2 > 0)
return $elem/@gml:id/string()
