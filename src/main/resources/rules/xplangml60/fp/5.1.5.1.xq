(:
  #%L
  XPlanung-Validierungsregeln - Implementierung der Konformitätsregeln des XPlanung-Standards als XQuery-Anweisungen
  %%
  Copyright (C) 2022 lat/lon GmbH, info@lat-lon.de, www.lat-lon.de
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-2.1.html>.
  #L%
  :)
declare default element namespace 'http://www.xplanung.de/xplangml/6/0';
declare namespace gml='http://www.opengis.net/gml/3.2';

for $h in //*[matches(local-name(), 'FP_Abgrabung|FP_AnpassungKlimawandel|FP_Aufschuettung|FP_DarstellungNachLandesrecht|FP_Gemeinbedarf|FP_GenerischesObjekt|FP_Gruen|FP_Kennzeichnung|FP_Landwirtschaft|FP_Nutzungsbeschraenkung|FP_PrivilegiertesVorhaben|FP_SchutzPflegeEntwicklung|FP_SpielSportanlage|FP_UnverbindlicheVormerkung|FP_VerEntsorgung')]
where (
	(
		$h/position/gml:Polygon or
		$h/position/gml:Surface or
		$h/position/gml:MultiSurface or
		$h/position/gml:LinearRing or
		$h/position/gml:PolygonPatch or
		$h/position/gml:Ring
	)
	and $h/ebene != '0'
	and not($h/flaechenschluss = 'false')
)
return $h/@gml:id/string()
