(:
  #%L
  XPlanung-Validierungsregeln - Implementierung der Konformitätsregeln des XPlanung-Standards als XQuery-Anweisungen
  %%
  Copyright (C) 2022 lat/lon GmbH, info@lat-lon.de, www.lat-lon.de
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-2.1.html>.
  #L%
  :)
declare default element namespace 'http://www.xplanung.de/xplangml/6/0';
declare namespace gml='http://www.opengis.net/gml/3.2';

for $h in //FP_BebauungsFlaeche[count(sondernutzung) > 1]
let $anzahlB1 := count($h/sondernutzung[FP_KomplexeSondernutzung/allgemein/text()='1000' or 
                                        FP_KomplexeSondernutzung/allgemein/text()='1100' or
                                        FP_KomplexeSondernutzung/allgemein/text()='1200' or
                                        FP_KomplexeSondernutzung/allgemein/text()='1300' or
                                        FP_KomplexeSondernutzung/allgemein/text()='1400'])

let $anzahlB2 := count($h/sondernutzung[FP_KomplexeSondernutzung/allgemein/text()='1500' or 
                                        FP_KomplexeSondernutzung/allgemein/text()='1600' or
                                        FP_KomplexeSondernutzung/allgemein/text()='16000' or
                                        FP_KomplexeSondernutzung/allgemein/text()='1650' or
                                        FP_KomplexeSondernutzung/allgemein/text()='16001' or
                                        FP_KomplexeSondernutzung/allgemein/text()='16002' or
                                        FP_KomplexeSondernutzung/allgemein/text()='1700' or
                                        FP_KomplexeSondernutzung/allgemein/text()='1800' or
                                        FP_KomplexeSondernutzung/allgemein/text()='1900' or
                                        FP_KomplexeSondernutzung/allgemein/text()='2000' or
                                        FP_KomplexeSondernutzung/allgemein/text()='2100' or
                                        FP_KomplexeSondernutzung/allgemein/text()='2200' or
                                        FP_KomplexeSondernutzung/allgemein/text()='2300' or
                                        FP_KomplexeSondernutzung/allgemein/text()='23000' or
                                        FP_KomplexeSondernutzung/allgemein/text()='2400' or
                                        FP_KomplexeSondernutzung/allgemein/text()='2500' or
                                        FP_KomplexeSondernutzung/allgemein/text()='2600' or
                                        FP_KomplexeSondernutzung/allgemein/text()='2700' or
                                        FP_KomplexeSondernutzung/allgemein/text()='2720' or
                                        FP_KomplexeSondernutzung/allgemein/text()='2800' or
                                        FP_KomplexeSondernutzung/allgemein/text()='2900' or
                                        FP_KomplexeSondernutzung/allgemein/text()='9999'])

where (
  $anzahlB1 > 0 and $anzahlB2 > 0
)                                        
 
return $h/@gml:id/string()







