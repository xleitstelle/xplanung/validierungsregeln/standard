(:
  #%L
  XPlanung-Validierungsregeln - Implementierung der Konformitätsregeln des XPlanung-Standards als XQuery-Anweisungen
  %%
  Copyright (C) 2019 lat/lon GmbH, info@lat-lon.de, www.lat-lon.de
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-2.1.html>.
  #L%
  :)
declare default element namespace 'http://www.xplanung.de/xplangml/5/1';
declare namespace gml='http://www.opengis.net/gml/3.2';

for $h in //*[matches(local-name(), 'RP_Achse|RP_Energieversorgung|RP_Entsorgung|RP_Freiraum|RP_Bodenschutz|RP_Erholung|RP_ErneuerbareEnergie|RP_Forstwirtschaft|RP_Gewaesser|RP_GruenzugGruenzaesur|RP_Hochwasserschutz|RP_Klimaschutz|RP_Kulturlandschaft|RP_Landwirtschaft|RP_NaturLandschaft|RP_NaturschutzrechtlichesSchutzgebiet|RP_RadwegWanderweg|RP_Rohstoff|RP_SonstigerFreiraumschutz|RP_Sportanlage|RP_Wasserschutz|RP_Funktionszuweisung|RP_GenerischesObjekt|RP_Grenze|RP_Kommunikation|RP_LaermschutzBauschutz|RP_Planungsraum|RP_Raumkategorie|RP_Siedlung|RP_Einzelhandel|RP_IndustrieGewerbe|RP_SonstigerSiedlungsbereich|RP_WohnenSiedlung|RP_SonstigeInfrastruktur|RP_SozialeInfrastruktur|RP_Sperrgebiet|RP_Verkehr|RP_Luftverkehr|RP_Schienenverkehr|RP_SonstVerkehr|RP_Strassenverkehr|RP_Wasserverkehr|RP_Wasserwirtschaft|RP_ZentralerOrt')]
where (
  $h/flaechenschluss != 'false'
  and
  $h/ebene != '0'
)
return $h/@gml:id/string()
